# byte_array

Byte array manipulation modelled after AS3 ByteArray class. https://help.adobe.com/en_US/FlashPlatform/reference/actionscript/3/flash/utils/ByteArray.html

## Usage

A simple usage example:

    import 'package:byte_array/byte_array.dart';

    main() {
      final bytes = ByteArray(6);
      bytes.writeFloat(33.6);
      bytes.writeUnsignedShort(5);

      print('length in bytes: ${bytes.length}');

      bytes.offset = 0;
      final float = bytes.readFloat();
      print(float);

      final short = bytes.readUnsignedShort();
      print(short);
    }

