import 'package:byte_array/byte_array.dart';
import 'package:test/test.dart';
import 'dart:typed_data' show Endian;

void main() {
  group('ByteArray', () {

    test('byte read/write range', () {
      final bytes = ByteArray(4)
        ..writeByte(-128)
        ..writeByte(-129)
        ..writeByte(127)
        ..writeByte(128)
        ..offset = 0;

      final values = List.generate(bytes.length, (_) => bytes.readByte());
      expect(values, orderedEquals([-128, 127, 127, -128]));
    });

    test('unsigned byte read/write range', () {
      final bytes = ByteArray(2)
        ..writeUnsignedByte(255)
        ..writeUnsignedByte(256)
        ..offset = 0;

      final values = List.generate(bytes.length, (_) => bytes.readUnsignedByte());
      expect(values, orderedEquals([255, 0]));
    });

    test('boolean read/write', () {
      final bytes = ByteArray(2)
        ..writeBoolean(true)
        ..writeBoolean(false)
        ..offset = 0;

      expect(bytes.readBoolean(), isTrue);
      expect(bytes.readBoolean(), isFalse);
    });

    test('short read/write range', () {
      final bytes = ByteArray(16)
        ..writeShort(-32768)
        ..writeShort(-32769)
        ..writeShort(32767)
        ..writeShort(32768)
        ..offset = 0;

      final values = List.generate(4, (_) => bytes.readShort());
      expect(values, orderedEquals([-32768, 32767, 32767, -32768]));
    });

    test('unsigned short read/write range', () {
      final bytes = ByteArray(4)
        ..writeUnsignedShort(65535)
        ..writeUnsignedShort(65536)
        ..offset = 0;

      final values = List.generate(2, (_) => bytes.readUnsignedShort());
      expect(values, orderedEquals([65535, 0]));
    });

    test('int read/write range', () {
      final bytes = ByteArray(16)
        ..writeInt(-2147483648)
        ..writeInt(-2147483649)
        ..writeInt(2147483647)
        ..writeInt(2147483648)
        ..offset = 0;

      final values = List.generate(4, (_) => bytes.readInt());

      expect(values, orderedEquals([-2147483648, 2147483647, 2147483647, -2147483648]));
    });

    test('unsigned int read/write range', () {
      final bytes = ByteArray(8)
        ..writeUnsignedInt(4294967295)
        ..writeUnsignedInt(4294967296)
        ..offset = 0;

      final values = List.generate(2, (_) => bytes.readUnsignedInt());

      expect(values, orderedEquals([4294967295, 0]));
    });

    test('long read/write range', () {
      final bytes = ByteArray(32)
        ..writeLong(-922337203685477581)
        ..writeLong(-922337203685477580)
        ..writeLong(9223372036854775807)
        ..writeLong(922337203685477580)
        ..offset = 0;

      final values = List.generate(4, (_) => bytes.readLong());

      expect(values, orderedEquals([-922337203685477581, -922337203685477580, 9223372036854775807, 922337203685477580]));
    });

    test('float read/write range', () {

      final floats = [-5.5, 23423.234, 89089.884, -343.233];

      final bytes = ByteArray(floats.length * 4);

      floats.forEach(bytes.writeFloat);

      bytes.offset = 0;
      final values = List.generate(floats.length, (_) => bytes.readFloat());

      for (var i = 0; i < floats.length; i++)
      {
        final x = floats[i];
        final y = values[i];

        expect(x, closeTo(y, .01));
      }
    });

    test('double read/write range', () {

      final dubs = [-5.5, 234623.234, 89089.884, -343.233];

      final bytes = ByteArray(dubs.length * 8);

      dubs.forEach(bytes.writeDouble);

      bytes.offset = 0;
      final values = List.generate(dubs.length, (_) => bytes.readDouble());

      for (var i = 0; i < dubs.length; i++)
      {
        final x = dubs[i];
        final y = values[i];

        expect(x, closeTo(y, .01));
      }
    });

    test('bytes read/write', () {

      // Test copying entire ByteArray
      final from = ByteArray(38)
        ..writeFloat(5.5)
        ..writeUnsignedByte(2)
        ..writeLong(-5)
        ..writeDouble(5.5)
        ..writeByte(-5)
        ..writeUnsignedLong(5)
        ..writeUnsignedInt(10)
        ..writeInt(-20)
        ..offset = 0;

      var to = ByteArray(from.length)
        ..writeBytes(from)
        ..offset = 0;

      expect(to.readFloat(), closeTo(from.readFloat(), .01));
      expect(to.readUnsignedByte(), equals(from.readUnsignedByte()));
      expect(to.readLong(), equals(from.readLong()));
      expect(to.readDouble(), closeTo(from.readDouble(), .01));
      expect(to.readByte(), equals(from.readByte()));
      expect(to.readUnsignedLong(), equals(from.readUnsignedLong()));
      expect(to.readUnsignedInt(), equals(from.readUnsignedInt()));
      expect(to.readInt(), equals(from.readInt()));

      // Test copying slice of ByteArray
      const len = 25;
      to = ByteArray(len)
        ..writeBytes(from, 5, len)
        ..offset = 0;

      from.offset = 5;

      expect(to.readLong(), equals(from.readLong()));
      expect(to.readDouble(), equals(from.readDouble()));
      expect(to.readByte(), equals(from.readByte()));
      expect(to.readUnsignedLong(), equals(from.readUnsignedLong()));
    });

    test('mixed read/write', () {
      final bytes = ByteArray(38)
        ..writeFloat(5.5)
        ..writeUnsignedByte(2)
        ..writeLong(-5)
        ..writeDouble(5.5)
        ..writeByte(-5)
        ..writeUnsignedLong(5)
        ..writeUnsignedInt(10)
        ..writeInt(-20)
        ..offset = 0;

      expect(bytes.readFloat(), closeTo(5.5, .01));
      expect(bytes.readUnsignedByte(), equals(2));
      expect(bytes.readLong(), equals(-5));
      expect(bytes.readDouble(), closeTo(5.5, .01));
      expect(bytes.readByte(), equals(-5));
      expect(bytes.readUnsignedLong(), equals(5));
      expect(bytes.readUnsignedInt(), equals(10));
      expect(bytes.readInt(), equals(-20));
    });

    test('length', () {
      final bytes = ByteArray(38)
        ..writeFloat(5.5)
        ..writeUnsignedByte(2)
        ..writeLong(-5)
        ..writeDouble(5.5)
        ..writeByte(-5)
        ..writeUnsignedLong(5)
        ..writeUnsignedInt(10)
        ..writeInt(-20)
        ..offset = 0;

      expect(bytes.length, equals(38));
    });

    test('offset', () {
      final bytes = ByteArray(38)
        ..writeFloat(5.5)
        ..writeUnsignedByte(2)
        ..writeLong(-5)
        ..writeDouble(5.5)
        ..writeByte(-5)
        ..writeUnsignedLong(5)
        ..writeUnsignedInt(10)
        ..writeInt(-20)
        ..offset = 0;

      bytes.offset = 34;
      expect(bytes.readInt(), equals(-20));

      bytes.offset = 13;
      expect(bytes.readDouble(), closeTo(5.5, .001));
    });

    test('invalid offset throws', () {
      final bytes = ByteArray(4);
      expect(() => bytes.offset = 8, throwsRangeError);
    });

    test('writing too far throws range error', () {
      final bytes = ByteArray(1);
      expect(() => bytes.writeInt(50), throwsRangeError);
    });

    test('change endianness', () {
      final bytes = ByteArray(2);
      bytes.endian = Endian.little;

      bytes
        ..writeShort(50)
        ..endian = Endian.big
        ..offset = 0;

      expect(bytes.readShort(), equals(12800));
    });

    test('bytes available', () {
      final bytes = ByteArray(38)
        ..writeFloat(5.5)
        ..writeUnsignedByte(2)
        ..writeLong(-5)
        ..writeDouble(5.5)
        ..writeByte(-5)
        ..writeUnsignedLong(5)
        ..writeUnsignedInt(10)
        ..writeInt(-20)
        ..offset = 0;

      expect(bytes.bytesAvailable, equals(bytes.length));

      bytes.offset = 34;
      expect(bytes.bytesAvailable, equals(4));

    });

    test('index operator: []', () {
      final bytes = ByteArray(4)
        ..[0] = 1
        ..[1] = 2
        ..[2] = 3
        ..[3] = 4;

      expect(bytes[0], equals(1));
      expect(bytes[1], equals(2));
      expect(bytes[2], equals(3));
      expect(bytes[3], equals(4));
    });

    test('+ operator', () {
      final b1 = ByteArray(2)
        ..[0] = 1
        ..[1] = 2;

      final b2 = ByteArray(2)
        ..[0] = 3
        ..[1] = 4;

      var bytes = b1 + b2;

      expect(bytes[0], equals(1));
      expect(bytes[1], equals(2));
      expect(bytes[2], equals(3));
      expect(bytes[3], equals(4));

      final b3 = ByteArray(2)
        ..[0] = 5
        ..[1] = 6;

      bytes += b3;

      expect(bytes[4], equals(5));
      expect(bytes[5], equals(6));
    });

    test('equality', () {
      final bytes = ByteArray(38)
        ..writeFloat(5.5)
        ..writeUnsignedByte(2)
        ..writeLong(-5)
        ..writeDouble(5.5)
        ..writeByte(-5)
        ..writeUnsignedLong(5)
        ..writeUnsignedInt(10)
        ..writeInt(-20)
        ..offset = 0;

      final bytes2 = ByteArray(38)
        ..writeFloat(5.5)
        ..writeUnsignedByte(2)
        ..writeLong(-5)
        ..writeDouble(5.5)
        ..writeByte(-5)
        ..writeUnsignedLong(5)
        ..writeUnsignedInt(10)
        ..writeInt(-20)
        ..offset = 0;

      expect(bytes, equals(bytes2));
      expect(bytes.hashCode, equals(bytes2.hashCode));
    });

    test('byte stream', () {
      final ints = [2,2,5,5,5,3,3];
      final bytes = ByteArray(ints.length);
      ints.forEach(bytes.writeUnsignedByte);

      bytes.offset = 0;
      for (final i in bytes.byteStream().skip(2).take(3)) {
        expect(i, 5);
      }
    });

  });
}
