# Changelog

## 0.1.5

- Use generic functions
- Use `implicit-casts: false` lint

## 0.1.4

- Update for Dart 2

## 0.0.1

- Initial version, created by Stagehand
